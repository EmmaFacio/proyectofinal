﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using AccesoDatos.Controlescolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.Controlescolar
{
   public  class MaestroManejador
    {
        private MaestrosAccesoDatos _maestrosAccesoDatos;
        public MaestroManejador()
        {
            _maestrosAccesoDatos = new MaestrosAccesoDatos();
        }
        public void Eliminar (string numeromaestro)
        {
          _maestrosAccesoDatos .Eliminar(numeromaestro);
        }
        public void Guardar(Maestros maestros)
        {
          _maestrosAccesoDatos.Guardar(maestros);
        }
        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A Z]*$|\s$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Usuarios>();
            list =_maestrosAccesoDatos.ObtenerLista(filtro);
            return list;
        }

        public Tuple<bool, string> EsmaestroValido(Maestros maestros
            )
        {
            string mensaje = "";
            bool valido = true;

            if (maestros.Nombrem.Length == 0)
            {
                mensaje = "El nombre de maestro es necesario";
                valido = false;
            }
            else if (NombreValido(maestros.Numeromaestro))
            {
                mensaje = "Escribe un formato valido para el nombre";
                valido = false;
            }
            else if (maestros.Nombrem.Length > 15)
            {
                mensaje = "La longitud para nombre de usuario es maximo 15 caracteres";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }

    }
}


    

