﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using System.Text.RegularExpressions;
using AccesoDatos.Controlescolar;

namespace LogicaNegocio.Controlescolar
{
    public class AlumnoManejador

    {
        public AlumnosAccesoDatos _alumnosAccesoDatos;
        public AlumnoManejador()
        {
            _alumnosAccesoDatos = new AlumnosAccesoDatos();
        }
        public void Guardar(Alumnos alumnos)
        {
            _alumnosAccesoDatos.Guardar(alumnos);
        }

        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A Z]*$|\s$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        public List<Alumnos> ObtenerLista(string filtro)
        {
            var list = new List<Alumnos>();
            list = _alumnosAccesoDatos.ObtenerLista(filtro);
            return list;
        }

        public Tuple<bool, string> EsalumnoValido(Alumnos alumno)
        {
            string mensaje = "";
            bool valido = true;

            if (alumno.Nombre.Length == 0)
            {
                mensaje = "El nombre de alumno es necesario";
                valido = false;
            }
            else if (NombreValido(alumno.Nombre))
            {
                mensaje = "Escribe un formato valido para el nombre";
                valido = false;
            }
            else if (alumno.Nombre.Length > 15)
            {
                mensaje = "La longitud para nombre de alumno es maximo 15 caracteres";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }

    }
}

