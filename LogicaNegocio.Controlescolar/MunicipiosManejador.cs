﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using AccesoDatos.Controlescolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.Controlescolar
{
    public class MunicipiosManejador
    {
        private MunicipiosAccesoDatos _municipiosAccessoDatos;
        public MunicipiosManejador()
        {
            _municipiosAccessoDatos = new MunicipiosAccesoDatos();
        }

        public List<Municipios> ObtenerLista(string filtro)
        {
            var list = new List<Municipios>();
            list = _municipiosAccessoDatos.ObtenerLista(filtro);
            return list;
        }

    }
}




    

