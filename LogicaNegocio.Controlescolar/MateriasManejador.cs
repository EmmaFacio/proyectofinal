﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using AccesoDatos.Controlescolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.Controlescolar
{
    public class MateriasManejador
    {
        private MateriaAccesoDatos _materiasAccesoDatos;
        public MateriasManejador()
        {
            _materiasAccesoDatos = new MateriaAccesoDatos();
        }
        public void Eliminar(int id)
        {
            _materiasAccesoDatos.Eliminar(id);
        }
        public void Guardar(Materias materias)
        {
            _materiasAccesoDatos.Guardar(materias);
        }

        private bool NombreValido(string Nombremateria)
        {

            var regex = new Regex(@"^[A Z]*$|\s$");
            var match = regex.Match(Nombremateria);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public List<Materias> ObtenerLista(string filtro)
        {
            var list = new List<Materias>();
            list = _materiasAccesoDatos.ObtenerLista(filtro);
            return list;
        }

        public Tuple<bool,String>EsmateriaValida(Materias materias)
        {
            string mensaje = "";
            bool valido = true;

            if (materias.NombreMateria.Length==0)
            {
                mensaje = "el nombre de la materia es necesario";
                valido = false;
            }
            else if (NombreValido(materias.NombreMateria))
            {
                mensaje = "Escribe un formato valido para el nombre";
                valido = false;
            }
            else if (materias.NombreMateria.Length>15)
            {
                mensaje = "la Longitud para nombre de usuario es maximo 15 caracteres";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);

        }


    }
}
