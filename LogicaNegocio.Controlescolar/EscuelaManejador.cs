﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using Extensiones.ControlEscolar;
using System.Text.RegularExpressions;
using System.IO;
using AccesoDatos.Controlescolar;

namespace LogicaNegocio.Controlescolar
{
    public class EscuelaManejador
    {
        private EscuelaAccesoDatos _escuelaAccessoDatos = new EscuelaAccesoDatos();
        private RutasManager _rutasManager;

        public EscuelaManejador(RutasManager rutaManager)
        {
            _rutasManager = rutaManager;
        }

        public void Guardar(escuela escuela)
        {
           // _escuelaAccessoDatos.Guardar(escuela);
        }

        public escuela GetEscuela()
        {
            return _escuelaAccessoDatos.Getescuela();
        }

        public bool CargarLogo(string filname)
        {
            var archivoNombre = new FileInfo(filname);

            if (archivoNombre.Length  > 500000)
            {
                return false;
            }
            return true;
        }


        public string GetNombreLogo(string filename)
        {
            var archivoImagen = new FileInfo(filename);
            return archivoImagen.Name;

        }
        public void LimpiarDocumento(int idescuela, string tipoDocuemtno)
        {
            string rutaRepositorio = "";
            string Extension = "";
            switch (tipoDocuemtno)
            {
                case "png":
                    rutaRepositorio = _rutasManager.RutasRepositoriosLogos;
                    Extension = "*.png";
                    break;
                case "jpg":
                    rutaRepositorio = _rutasManager.RutasRepositoriosLogos;
                    break;

            }
            string ruta = Path.Combine(rutaRepositorio, idescuela.ToString());
            if (Directory.Exists(ruta))
            {
                var obtenerArchivos = Directory.GetFiles(ruta, Extension);
                FileInfo archivoAnterior;
                if (obtenerArchivos.Length != 0) ;
                {
                    archivoAnterior = new FileInfo(obtenerArchivos[0]);

                    if (obtenerArchivos.Exists)
                    {
                        archivoAnterior.Delete();

                    }
                }
            }
        }
            
            public void GuardarLogo(string fileName, int Idescuela)
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    var archivoDocument = new FileInfo(fileName);
                string ruta = Path.Combine(_rutasManager.RutasRepositoriosLogos, Idescuela.ToString());
                if (Directory.Exists(ruta))
                    {
                        var ObtenerArchivos = Directory.GetFiles(ruta);
                        FileInfo archivoAnterior;
                        if (ObtenerArchivos.Length != 0)
                        {
                            archivoAnterior = new FileInfo(ObtenerArchivos[0]);

                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                            }
                        }
                        else
                            archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                    }
                }
                else
                {
                 
                     _rutasManager.CrearRepositorioLogosEscuela(Idescuela);
                    archivoDocument.Copy(Path.Combine(ruta, archivoDocument.Name));
                }
            }
        }
    }
}




