﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Controlescolar
{
    public class Materias
    {
        private int _id;
        private string _NombreMateria;
        private int _fk_MAnterior;
        private int _fk_MDespues;

        public int Id { get => _id; set => _id = value; }
        public string NombreMateria { get => _NombreMateria; set => _NombreMateria = value; }
        public int Fk_MAnterior { get => _fk_MAnterior; set => _fk_MAnterior = value; }
        public int Fk_MDespues { get => _fk_MDespues; set => _fk_MDespues = value; }
    }
}
