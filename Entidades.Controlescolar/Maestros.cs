﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Controlescolar
{
    public class Maestros
    {

        private string _numeromaestro;
        private string _nombrem;
        private string _apellidopaterno;
        private string _apellidomaterno;
        private string _fechanacimiento;
        private string _estado;
        private string _ciudad;
        private string _sexo;
        private string _correo;
        private string _tarjeta;
        private string _nivel_estudio;

        public string Numeromaestro { get => _numeromaestro; set => _numeromaestro = value; }
        public string Nombrem { get => _nombrem; set => _nombrem = value; }
        public string Apellidopaterno { get => _apellidopaterno; set => _apellidopaterno = value; }
        public string Apellidomaterno { get => _apellidomaterno; set => _apellidomaterno = value; }
        public string Fechanacimiento { get => _fechanacimiento; set => _fechanacimiento = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Correo { get => _correo; set => _correo = value; }
        public string Tarjeta { get => _tarjeta; set => _tarjeta = value; }
        public string Nivel_estudio { get => _nivel_estudio; set => _nivel_estudio = value; }
    }
}

