﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Controlescolar
{
   public  class Municipios
    {
        private int _id_municipio;
        private string _nombre;

        public int Id_municipio { get => _id_municipio; set => _id_municipio = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
    }
}
