﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Controlescolar
{
    public class Alumnos
    {
        private  string _Numero_control;
        private  string _Nombre;
        private string  _Apellidopaterno;
        private string _Apellidomaterno;
        private string _Sexo;
        private string _Fecha_nacimiento;
        private string _correo_electronico;
        private string _Telefono;
        private string _Estados;
        private string _municipios;
        private string _domicilio;

        public string Numero_control { get => _Numero_control; set => _Numero_control = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Apellidopaterno { get => _Apellidopaterno; set => _Apellidopaterno = value; }
        public string Apellidomaterno { get => _Apellidomaterno; set => _Apellidomaterno = value; }
        public string Sexo { get => _Sexo; set => _Sexo = value; }
        public string Fecha_nacimiento { get => _Fecha_nacimiento; set => _Fecha_nacimiento = value; }
        public string Correo_electronico { get => _correo_electronico; set => _correo_electronico = value; }
        public string Telefono { get => _Telefono; set => _Telefono = value; }
        public string Estados { get => _Estados; set => _Estados = value; }
        public string Municipios { get => _municipios; set => _municipios = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
    }
}
