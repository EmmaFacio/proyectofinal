﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;

namespace Controlescolar
{
    public partial class MaestrosModal : Form
    {
        private MaestroManejador _maestromanejador;
        private Maestros _maestros;
        private bool _isEnabledBinding = false;
        public MaestrosModal()
        {
            InitializeComponent();
            _maestromanejador = new MaestroManejador();
            _maestros = new Maestros();
            _isEnabledBinding = true;
            BindingMaestros();
        }

        public MaestrosModal(Maestros maestros)
        {
            InitializeComponent();
            _maestromanejador = new MaestroManejador();
            _maestros = new Maestros();
            _maestros = maestros;
            BindingMaestrosReload();
            _isEnabledBinding = true;
        }

        private void BindingMaestrosReload()
        {
            textBox1.Text = _maestros.Numeromaestro;
            textBox2.Text = _maestros.Nombrem;
            textBox3.Text = _maestros.Apellidopaterno;
            textBox4.Text = _maestros.Apellidomaterno;
            textBox5.Text = _maestros.Fechanacimiento;
            textBox6.Text = _maestros.Estado;
            textBox7.Text = _maestros.Ciudad;
            textBox8.Text = _maestros.Sexo;
            textBox9.Text = _maestros.Correo;
            textBox10.Text = _maestros.Tarjeta;
            textBox11.Text = _maestros.Nivel_estudio;
        }

        private void Label10_Click(object sender, EventArgs e)
        {

        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            BindingMaestros();

            if (validarMaestros())
            {
                Guardar();
                this.Close();

            }
        }

        private void Guardar()
        {
            _maestromanejador.Guardar(_maestros);
        }
        private bool validarMaestros()
        {
            var res = _maestromanejador.EsmaestroValido(_maestros);
            if (!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }

            return res.Item1;
        }

        private void BindingMaestros()
        {
            if (_isEnabledBinding)
            {
                if (_maestros.Numeromaestro == 0)
                {
                    _maestros.Numeromaestro =0;
                }
                _maestros.Numeromaestro = textBox1.Text;
                _maestros.Nombrem = textBox2.Text;
                _maestros.Apellidopaterno = textBox3.Text;
                _maestros.Apellidomaterno = textBox4.Text;
                _maestros.Fechanacimiento = textBox5.Text;
                _maestros.Estado = textBox6.Text;
                _maestros.Ciudad = textBox7.Text;
                _maestros.Sexo = textBox8.Text;
                _maestros.Correo = textBox9.Text;
                _maestros.Tarjeta = textBox10.Text;
                
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Form5 frm5 = new Form5();

            frm5.Show();

        }
    }
}


