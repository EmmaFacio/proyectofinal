﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;

namespace Controlescolar
{
    public partial class Alumno : Form
    {

        private AlumnoManejador _alumnoManejador;
        private Alumnos _alumno;
        private EstadosManejador _estadosManejador;
        private MunicipiosManejador _municipiosManejador;
        private bool _isEnableBinding = false;

        public Alumno()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumno = new Alumnos();
            _estadosManejador = new EstadosManejador();
            _municipiosManejador = new MunicipiosManejador();
            _isEnableBinding = true;
            //BindingAlumno();
        }
        public Alumno(Alumnos alumno)
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumno = new Alumnos();
            _estadosManejador = new EstadosManejador();
            _municipiosManejador = new MunicipiosManejador();
            _alumno = _alumno;
            BindingAlumnoReload();
            _isEnableBinding = true;
        }

        private void BindingAlumnoReload()
        {
            txtNumeroControl.Text = _alumno.Numero_control;
            txtNombre.Text = _alumno.Nombre;
            txtApellidoP.Text = _alumno.Apellidopaterno;
            txtApellidoM.Text = _alumno.Apellidomaterno;
            txtCorreo.Text = _alumno.Sexo;
            txtFechaNacimiento.Text = _alumno.Fecha_nacimiento;
            txtTelefono.Text = _alumno.Telefono;
            CmbEstados.Text = _alumno.Estados;
            CmbMunicipios.Text = _alumno.Municipios;

        }

        private void Alumno_Load(object sender, EventArgs e)
        {

            var Estados = _estadosManejador.ObtenerLista();
            foreach (var estados in Estados)
            {
                CmbEstados.Items.Add(estados.Nombre);

            }


 }

        private void BindingAlumno()
        {
            if (_isEnableBinding)
            {
                if (_alumno.Numero_control=="")
                {
                    _alumno.Numero_control = "";
                }

                _alumno.Numero_control = txtNumeroControl.Text;
                _alumno.Nombre = txtNombre.Text;
                _alumno.Apellidopaterno = txtApellidoP.Text;
                _alumno.Apellidomaterno = txtApellidoM.Text;
                _alumno.Correo_electronico = txtCorreo.Text;
                _alumno.Fecha_nacimiento = txtFechaNacimiento.Text;
                _alumno.Telefono = txtTelefono.Text;
                _alumno.Estados = CmbEstados.Text;
                _alumno.Municipios = CmbMunicipios.Text;
                }
              }
        private void Guardar()
        {
            _alumnoManejador.Guardar(_alumno);
        }
         private void BtnGuardar_Click(object sender, EventArgs e)
        {

            BindingAlumno();
            Guardar();
            MessageBox.Show("Guardado Correctamnete!!");
            this.Close();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmbMunicipios_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CmbEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingAlumno();
            CmbMunicipios.Items.Clear();
            string filtro = "";
            var Estados = _estadosManejador.ObtenerLista();
            foreach (var item in Estados)
            {
                if (item.Nombre.Equals(CmbEstados,Text))
                {
                    filtro = item.Codigo;
                }
            }
            var Municipios = _municipiosManejador.ObtenerLista(filtro);
            foreach(var municipio in Municipios)
            {
                CmbMunicipios.Add(municipio.Nombre);
            }
        }

        private void TxtNumeroControl_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtApellidoP_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtApellidoM_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtSexo_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtFechaNacimiento_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtCorreo_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }
    }
}
