﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controlescolar
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 frmUsuarios = new Form1();
            frmUsuarios.ShowDialog();
        }

        private void Principal_Load(object sender, EventArgs e)
        {

        }

        private void AlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void EscuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEscuela frmEscuela = new frmEscuela();
            frmEscuela.ShowDialog();
        }
    }
}
