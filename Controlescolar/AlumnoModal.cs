﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;

namespace Controlescolar
{
    public partial class Form2 : Form
    {

        private AlumnoManejador _alumnoManejador;
        private Alumnos _alumnos;
        private bool _isEnabledBinding = false;
        public Form2()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
             _alumnos = new Alumnos();
            _isEnabledBinding = true;
            BindingAlumnos();
        }

        public Form2(Alumnos alumnos)

        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumnos = new Alumnos();
            _alumnos = alumnos;
            BindingAlumnosReload();
            _isEnabledBinding = true;
        }

        private void BindingAlumnosReload()
        {
            //textBox1.Text = _alumnos.Numero_control;
            textBox2.Text = _alumnos.Nombre;
            textBox3.Text = _alumnos.Apellidopaterno;
            textBox4.Text = _alumnos.Apellidomaterno;
            textBox5.Text = _alumnos.Sexo;
            textBox6.Text = _alumnos.Fecha_nacimiento;
            textBox7.Text = _alumnos.Correo_electronico;
        }
        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            BindingAlumnos();

            if (validarAlumno())
            {
                Guardar();
                this.Close();
             
            }
        }

        private void Guardar()
        {
            _alumnoManejador.Guardar(_alumnos);
        }

        private bool validarAlumno()
        {
            var res = _alumnoManejador.EsalumnoValido(_alumnos);
            if(!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }

            return res.Item1; 

        }

        private void BindingAlumnos()
        {
            if(_isEnabledBinding)
            {
               if(_alumnos.Numero_control==0)
                {
                    _alumnos.Numero_control = 0;
                }
                //_alumnos.Numero_control=textBox1.Text;
                _alumnos.Nombre = textBox2.Text;
                _alumnos.Apellidopaterno = textBox3.Text;
                _alumnos.Apellidomaterno = textBox4.Text;
                _alumnos.Sexo = textBox5.Text;
                _alumnos.Fecha_nacimiento = textBox6.Text;
                _alumnos.Correo_electronico = textBox7.Text;
                _alumnos.Domicilio = textBox8.Text;

            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
        }

        private void TextBox3_TextChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
        }

        private void TextBox4_TextChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
        }

        private void TextBox5_TextChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
        }

        private void TextBox6_TextChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
        }

        private void TextBox7_TextChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
        }

        private void TextBox8_TextChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
