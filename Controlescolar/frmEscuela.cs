﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;
//using Extension.ControlEscolar;

namespace Controlescolar
{
    public partial class frmEscuela : Form
    {
          private void frmEscuela_Load(object sender, EventArgs e)
        {
            btnBuscarImagen.Enabled = false;
            btnEliminarImagen.Enabled = false;
           btnGuardar.Enabled = false;
            btnCancelar.Enabled = false;
           

        }

        private EscuelaManejador _escuelaManejador;
        private OpenFileDialog _dialogCargarLogo;
        public bool _isImagenClear = false;
        private bool _isEnabledBinding = false;
        private escuela _escuela;
        private RutasManager _rutasManager;

        public frmEscuela()
        {
            InitializeComponent();
            _dialogCargarLogo = new OpenFileDialog();
            _rutasManager = new RutaManager(Application.StartupPath);
            _escuelaManejador = new EscuelaManejador (_rutasManager);
            _escuela = new escuela();
            if (string.IsNullOrEmpty(_escuela.Idescuela.ToString())) 
            {
                LoadEntity();
            }
            _isEnabledBinding = true;
        }

        private void LoadEntity()
        {
            txtNombre.Text = _escuela.Nombre;
            txtDirector.Text = _escuela.Director;
            picLogo.ImageLocation = null;

            if (!string.IsNullOrEmpty(_escuela.Logo) && string.IsNullOrEmpty(_dialogCargarLogo.FileName))
            {
                picLogo.ImageLocation = _rutasManager.RutaLogoEscuela(_escuela);
            }
        }

        private void BingEntity()
        {
            _escuela.Nombre = txtNombre.Text;
            _escuela.Director = txtDirector.Text;
        }



        private void CargarLogo()
        {
            _dialogCargarLogo.Filter = "Imagen tipo (*.png)|*.png|*.png|Imagen Tipo (*.jpg)|*.jpg";
            _dialogCargarLogo.Title = "Cargue un archivo";
            _dialogCargarLogo.ShowDialog();

            if (_dialogCargarLogo.FileName != "")
            {
                if (_escuelaManejador.Cargarimagen(_dialogCargarLogo.FileName))
                {
                    picLogo.ImageLocation = _dialogCargarLogo.FileName;
                    _isImagenClear = false;
                }
                else
                {
                    MessageBox.Show("No se pueden subir imágenes de más de 5 MB");
                }
            }
        }
       
                private void BtnModificar_Click(object sender, EventArgs e)
        {

            btnBuscarImagen.Enabled = true;
            btnEliminarImagen.Enabled = true;
            btnGuardar.Enabled = true;
            btnCancelar.Enabled = true;
        }

        public void Save()
        {
            try
            {

                if (picLogo.Image != null)
                {
                    if (!string.IsNullOrEmpty(_dialogCargarLogo.FileName))
                    {
                        _escuela.Logo = _escuelaManejador.GetNombreLogo(_dialogCargarLogo.FileName);
                    }
                    if (!string.IsNullOrEmpty(_escuela.Logo))
                    {
                        _escuelaManejador.GuardarLogo(_dialogCargarLogo.FileName, 1);
                        _dialogCargarLogo.Dispose();
                    }
                }
                else
                {
                    _escuela.Logo = string.Empty;
                }
                if (_isImagenClear)
                {
                    _escuelaManejador.LimpiarDocumento(1, "png");
                    _escuelaManejador.LimpiarDocumento(1, "jpg");
                }
                _escuelaManejador.Guardar(_escuela);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }
        }

        private void BtnBuscarImagen_Click(object sender, EventArgs e)
        {
            CargarLogo();
        }

        private void BtnEliminarImagen_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea eliminar?", "Eliminar", MessageBoxButtons.OKCancel)) == DialogResult.OK)
            {
                picLogo.ImageLocation = null;
                _isImagenClear = true;
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            btnBuscarImagen.Enabled = false;
            btnEliminarImagen.Enabled = false;
            btnGuardar.Enabled = false;
            btnCancelar.Enabled = false;
            if ((MessageBox.Show("¿Seguro que desea Salir?", "Bruh", MessageBoxButtons.OKCancel)) == DialogResult.OK)
            {
                this.Close();
            }

        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {
            BingEntity();
        }

        private void TxtDirector_TextChanged(object sender, EventArgs e)
        {
            BingEntity();
        }

        private void FrmEscuela_Load_1(object sender, EventArgs e)
        {

        }
    }
    }

