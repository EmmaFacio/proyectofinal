﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;


namespace Controlescolar
{
    public partial class Form3 : Form
    {

        AlumnoManejador _alumnoManejador;
        Alumnos _alumnos;
        public Form3()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumnos = new Alumnos();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            buscarAlumnos("");
        }

        private void buscarAlumnos(string numero_control)
        {
            dgvAlumnos.DataSource = _alumnoManejador.ObtenerLista(numero_control);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarAlumnos(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deseas eliminar este registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    buscarAlumnos("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Eliminar()
        {
            //string numero_control = Convert.ToInt32(dgvAlumnos.CurrentRow.Cells["numero_control"].Value);
            //_alumnoManejador.Eliminar(numero_control);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            buscarAlumnos("");
        }

        private void DgvAlumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingAlumno();
            Form3 form3 = new Form3 (_alumnos);
            form3.ShowDialog();
            buscarAlumnos("");
        }

        private void BindingAlumno()
        {
            //_alumnos.Numero_control =  Convert.ToInt32(dgvAlumnos.CurrentRow.Cells["numero_control"].Value);
            _alumnos.Nombre = dgvAlumnos.CurrentRow.Cells["nombre"].Value.ToString();
            _alumnos.Apellidopaterno = dgvAlumnos.CurrentRow.Cells["Apellidopaterno"].Value.ToString();
            _alumnos.Apellidomaterno = dgvAlumnos.CurrentRow.Cells["apellidomaterno"].Value.ToString();
            _alumnos.Sexo = dgvAlumnos.CurrentRow.Cells["sexo"].Value.ToString();
            _alumnos.Fecha_nacimiento = dgvAlumnos.CurrentRow.Cells["fecha_nacimiento"].Value.ToString();
            _alumnos.Correo_electronico = dgvAlumnos.CurrentRow.Cells["correo_electronico"].Value.ToString();
            _alumnos.Telefono = dgvAlumnos.CurrentRow.Cells["telefono"].Value.ToString();
            _alumnos.Estados = dgvAlumnos.CurrentRow.Cells["estados"].Value.ToString();
            _alumnos.Municipios = dgvAlumnos.CurrentRow.Cells["municipios"].Value.ToString();
            _alumnos.Domicilio = dgvAlumnos.CurrentRow.Cells["domicilio"].Value.ToString();
        
    
            
        }
    }
}
