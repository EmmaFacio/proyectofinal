﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using System.IO;

namespace Extensiones.ControlEscolar
{
    public class RutasManager
    {
        private string _appPath;
        private const string LOGOS = "logos";

        public RutasManager(string appPath)//crear constructor para que se ejecute darle la ruta especificada de la parte inicial
        {
            _appPath = appPath;
        }

        public string RutasRepositoriosLogos
        {
            get { return Path.Combine(_appPath, LOGOS); }//retorname un string de la pac donde estes trabajando y la constatnte logos
        }


        public void CrearRepositoriosLogos()
        {
            if (!Directory.Exists(RutasRepositoriosLogos))//si el directorio logo esxiste en esta ruta lo crea y si no no.
            {
                Directory
                    .CreateDirectory(RutasRepositoriosLogos);  // aqui si no existe lo crea ruta reposotorios logos. 
            }
        }

        public void CrearRepositorioLogosEscuela(int Idescuela)
        {
            CrearRepositoriosLogos();
            string ruta = Path.Combine(RutasRepositoriosLogos, Idescuela.ToString());
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }

        }


        public string RutaLogoEscuela(escuela escuela)
        {
            return Path.Combine(RutasRepositoriosLogos, escuela.Idescuela.ToString(), escuela.Logo);
        }
    }
}

            
        


    

