﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBd;
using Entidades.Controlescolar;
using System.Data;



namespace AccesoDatos.Controlescolar
{
    class EscuelaAccesoDatos
    {
        Conexion _conexion;

        public EscuelaAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Guardar(escuela escuela)
        {
            if (escuela.Idescuela == 0)
            {
                string cadena = string.Format("Insert into escuela values(null,'{0}','{1}','{2}')",
                    escuela.Nombre, escuela.Director, escuela.Logo);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update escuela set nombre= '{0}', director= '{1}', logo= '{2}' "
                    + "where idescuela='{3}'",
                    escuela.Nombre, escuela.Director, escuela.Logo, escuela.Idescuela);
                _conexion.EjecutarConsulta(cadena);
            }
        }

        public  escuela GetEscuela()
        {
            var ds = new DataSet();
            string consulta = "select * from escuela";
            ds = _conexion.ObtenerDatos(consulta, "escuela");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var escuela = new escuela();
            foreach (DataRow row in dt.Rows)
            {
                escuela.Idescuela = Convert.ToInt32(row["idescuela"]);
                escuela.Nombre = row["nombre"].ToString();
                escuela.Director = row["director"].ToString();
                escuela.Logo = row["logo"].ToString();
            }
            return escuela;
        }
    }
}











