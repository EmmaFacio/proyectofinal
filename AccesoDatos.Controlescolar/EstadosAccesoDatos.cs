﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
   public  class EstadosAccesoDatos
    {
        Conexion _conexion;

        public EstadosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);

        }


        public List<Estados> ObtenerLista()
        {
            var list = new List<Estados>();
            string consulta = string.Format("Select * from estados");
            var ds = _conexion.ObtenerDatos(consulta, "estados");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var estados = new Estados
                {
                    Codigo = row["codigo"].ToString(),
                    Nombre = row["nombre"].ToString(),

                };

                list.Add(estados);
            }


            return list;
        }
    }
}


    

