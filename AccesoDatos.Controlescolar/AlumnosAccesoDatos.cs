﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
    class AlumnosAccesoDatos
    {
        Conexion _conexion;
        public AlumnosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Eliminar(string Numero_control)
        {
            string cadena=string.Format("Delete from alumnos where numero_control={0}", Numero_control);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(Alumnos alumnos)
        {

            if(Convert.ToInt32(alumnos.Numero_control)==0) 
            {
                string cadena = string.Format("Insert into alumnos values(null, '{0}','{1}','{2}')", alumnos.Numero_control, alumnos.Apellidopaterno, 
               alumnos.Apellidomaterno,alumnos.Sexo,alumnos.Fecha_nacimiento,alumnos.Correo_electronico,alumnos.Telefono,alumnos.Estados
               ,alumnos.Municipios,alumnos.Domicilio);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update alumnos set Numero_control= '{0}',Nombre= '{1}', apellidopaterno= " +
                    "'{2}',apellidomaterno='{3}',sexo='{4},fecha_nacimiento='{5}',correo_electronico='{6}',Telefono='{7},estados='{8},municipios=" +
                    "'{9}',domicilio='{10}'",
                alumnos.Numero_control, alumnos.Nombre, alumnos.Apellidopaterno, alumnos.Apellidomaterno
                ,alumnos.Sexo,alumnos.Fecha_nacimiento,alumnos.Correo_electronico,alumnos.Telefono,alumnos.Estados,alumnos.Municipios
                ,alumnos.Domicilio);
                _conexion.EjecutarConsulta(cadena);
            }


        }

        public List<Alumnos> ObtenerLista(string filtro)
        {
            var list = new List<Alumnos>();
            string consulta = string.Format("Select * from alumnos nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "alumno");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumnos
                {
                    Numero_control =row["numero_control"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Sexo=row["sexo"].ToString(),
                    Fecha_nacimiento=row["Fecha_nacimiento"].ToString(),
                    Correo_electronico=row["correo_electronico"].ToString(),
                    Telefono=row["telefono"].ToString(),
                    Estados=row["estados"].ToString(),
                    Municipios=row["municipios"].ToString(),
                    Domicilio=row["domicilio"].ToString()
                };

                list.Add(alumno);
            }


            return list;
        }
    }
}
