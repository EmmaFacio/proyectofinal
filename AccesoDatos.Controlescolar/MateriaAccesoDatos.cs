﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
    public class MateriaAccesoDatos
    {
        Conexion _conexion;

            public MateriaAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);

        }
        public void Eliminar(int id)
        {
            string cadena = string.Format("Delete from materias where id={0}", id);
            _conexion.EjecutarConsulta(cadena);
        }

        public void Guardar(Materias materia)
        {
            if (materia.Id == 0)
            {
                string cadena = string.Format("Insert into materia values(null, '{0}','{1}','{2}',{3})", materia.NombreMateria, materia.Fk_MAnterior, materia.Fk_MDespues);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update materia set nombre= '{0}', MAnterior= '{1}', MDespues= '{2}' where id='{3}'", materia.NombreMateria, materia.Fk_MAnterior, materia.Fk_MDespues, materia.Id);
                _conexion.EjecutarConsulta(cadena);
            } }


        //public string MateriaAnterior 

       public List<Materias>ObtenerLista(string filtro)
            {
            var list = new List<Materias>();
            string consulta = string.Format("Select * from materias where NombreMateria like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "usuarios");
            var dt= ds.Tables[0];

            foreach (DataRow row in dt.Rows)
             {
                var materias = new Materias
                {
                   Id = Convert.ToInt32(row["id"]),
                    NombreMateria = row["nombre"].ToString(),
                    Fk_MAnterior = Convert.ToInt32(row["MAnterior"]),
                    Fk_MDespues = Convert.ToInt32(row["MDespues"]),
                };
                list.Add(materias);
            }
            return list;
        }



        
    }
}
