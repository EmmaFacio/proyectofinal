﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
   public  class MaestrosAccesoDatos
    {
        Conexion _conexion;

        public MaestrosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(string numeromaestro)
        {
             string cadena = string.Format("Delete from maestros where numeromaestro={0}", numeromaestro);
            _conexion.EjecutarConsulta(cadena);
        }

        public void Guardar(Maestros maestros)
        {
            if (maestros.Numeromaestro == "")
            {
                string cadena = string.Format("Insert into maestros values(null, '{0}','{1}','{2}')", maestros.Numeromaestro, maestros.Nombrem, maestros.Apellidopaterno,
                    maestros.Apellidomaterno, maestros.Fechanacimiento, maestros.Estado, maestros.Ciudad, maestros.Sexo, maestros.Correo, maestros.Tarjeta, maestros.Nivel_estudio);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update maestros set numeromaestro= '{0}', apellidopaterno= '{1}', apellidomaterno= '{2}' where numeromaestro='{3}'", maestros.Numeromaestro,
                maestros.Nombrem, maestros.Apellidopaterno, maestros.Numeromaestro);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Maestros>();
            string consulta = string.Format("Select * from maestros where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "maestros");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var maestros = new Maestros
                {
                    Numeromaestro = "",
                    Nombrem = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Fechanacimiento = row["fechanacimiento"].ToString(),
                    Estado = row["estado"].ToString(),
                    Ciudad = row["ciudad"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Correo = row["correo"].ToString(),
                    Tarjeta = row["Tarjeta"].ToString(),
                    Nivel_estudio = row["nivel_estudios"].ToString()

                };
                list.Add(maestros);
            }
            return list;
                 }

             }
        }

    

