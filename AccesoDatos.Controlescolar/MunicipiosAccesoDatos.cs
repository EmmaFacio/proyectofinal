﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
    public class MunicipiosAccesoDatos
    {
        Conexion _conexion;

        public MunicipiosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);

        }

       

        public List<Municipios> ObtenerLista(string filtro)
        {
            var list = new List<Municipios>();
            string consulta = string.Format("Select * from municipios");
            var ds = _conexion.ObtenerDatos(consulta, "municipios");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var municipios = new Municipios
                {
                    Id_municipio = Convert.ToInt32(row["id_municipio"]),
                    Nombre = row["nombre"].ToString(),
                  
                };

                list.Add(municipios);
            }


            return list;
        }
    }
}
    


